# Plugin Conflict

Bash script to resolve WordPress plugin conflicts. The default action is to switch plugins on and off one by one and curl the homepage to find out if the issue is resolved. <br />

usage : pluginConflict [-h,--help] [--url=CustomUrl] [--successCode=CustomHTTPCode] [-i, --interactive] [-r, --reset] [-d, --deactivate] <br />
	  
 optional arguments : <br />
 -h, --help <br />
	&ensp;show this help message and exit <br />
 -u, --url   <br />
	&ensp;Set which URL will be checked in normal mode. Default is set with "wp option get home".<br />
 -s, --successCode <br />
	&ensp;Set which HTTP code translates as the issue is resolved. Default is 200.<br />
 -i, --interactive <br />
	&ensp;In interactive mode the scipt doesnt check the HTTP code to determine if the issue is resolved but pauses between plugin deactivations for user input.<br />
 -r, --reset <br />
	&ensp;When finished set plugins status (active/inactive) as they were before script execution.<br />
 -d, --deactivate <br />
	&ensp;By default plugins are switched off one by one, check the URL and switched back on if the issue is not resolved. This option leaves them deactivated. <br />