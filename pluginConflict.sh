#!/bin/bash
#Default settings
url=$(wp option get home --skip-plugins --skip-themes)
successCode=200
errorCode=503
fixxed=0
help=0
interactive=0
reset=0
deactivate=0
errorMode=0

#Get active/inactive plugins into arrays
inactive=( $(wp plugin list --skip-plugins --skip-themes |grep 'inactive'|sed -e '1d;s/\t/ /g'|cut -d" " -f1) );
active=( $(wp plugin list --skip-plugins --skip-themes |grep -v 'inactive'|sed -e '1d;s/\t/ /g'|cut -d" " -f1) );
declare -a inactive
declare -a active

#Error Mode 
#	Curl URL and return 0 if HTTP return code is different than error code specified
#Default Mode
# Curl URL and return 0 if HTTP return code is the same as successCode
function checkHTTPCode () {
	#curl URL and save the last HTTP code received (redirects) 
	returnCode=$(curl -ILs $url |grep HTTP|tail -1|sed -E 's/.* ([0-9]*) .*/\1/g');
	
	#Error Mode
	if [[ $errorMode -eq 1 ]]
	then
		if [[ $returnCode -eq $errorCode ]]
		then
			return 1
		fi
		return 0 
	#Default Mode 
	else 
		if [[ $returnCode -eq $successCode ]]
		then
		  return 0
		fi
		return 1
	fi
}
#Interactive mode
#  Deactivate plugins 1 by 1 
#  Wait for user to check manually 
#  Wait user input to continue or stop
function interactiveMode(){
	read -n 1 -p "Continue? y/n " userInput
	echo " " #leave new line after input 

	if [[ "$userInput" != "y" ]]
	then
		#If reset option is not set up
		#Notify user that the plugin will be left deactivated
		if [[ reset -eq 0 ]]
		then
			echo "Warning: plugin $i is left deactivated"
		fi
		return 1
	fi
	return 0 
}
#Automated Mode mode 
#  Deactivate plugins 1 by 1 and check HTTP return code
#  Reactivate if issue is not resolved. 
#  Break loop execution if conflict is resolved. 
function automatedMode() {
	checkHTTPCode
	return $?
}

#Reset plugins status as they were before script execution
function resetPlugins (){
	for i in "${inactive[@]}"
	do
		wp plugin deactivate $i --skip-plugins --skip-themes &> /dev/null
	done
	
	for i in "${active[@]}"
	do
		wp plugin activate $i --skip-plugins --skip-themes &> /dev/null
	done
}


function checkPluginConflicts () {
	for i in "${active[@]}"
	do
		wp plugin deactivate $i --skip-plugins --skip-themes 1>/dev/null
		echo "Deactivated $i plugin"
		#Interactive mode 
		if [[ $interactive -eq 1 ]]
		then
			interactiveMode
			if [[ $? -eq 1 ]];
			then
				break
			fi
		#Default Mode 
		else
			automatedMode
			if [[ $? -eq 0 ]] 
			then
			   echo "Conflict was resolved with $i deactivated"
			   break
			fi
		fi
		#By default reactivate each plugin which we deactivated
		#if deactivate=1 we dont activate 
		if [[ $deactivate -eq 0 ]]
		then
			echo "Activating plugin $i"
			wp plugin activate $i --skip-plugins --skip-themes 1>/dev/null
		fi
	done
}
#Get command line arguments
for i in "$@"; do
	case $i in
	-h|--help)
	  help=1
	  shift # past argument=value
	  ;;
	-u=*|--url=*)
	  url="${i#*=}"
	  shift # past argument=value
	  ;;
	-s=*|--successCode=*)
	  successCode="${i#*=}"
	  shift # past argument=value
	  ;;
	-i|--interactive)
	  interactive=1
	  shift # past argument no value
	  ;;
	-r|--reset)
	  reset=1
	  shift # past argument no value
	  ;;
	-d|--deactivate)
	  deactivate=1
	  shift # past argument no value
	  ;;
	-e=*|--errorCode=*)
	  errorCode="${i#*=}"
	  errorMode=1
	  shift # past argument=value
	  ;;
	-*|--*)
	  echo "Unknown option $i"
	  exit 1
	  ;;
	*)
	  ;;
	esac
done

if [[ help -eq 1 ]]
then
	echo "usage : pluginConflict [-h,--help] [--url=CustomUrl] [--successCode=CustomHTTPCode] [-i, --interactive] [-r, --reset] [-d, --deactivate]"
	echo " "
	echo "optional arguments : "
	echo "-h, --help show this help message and exit"
	echo "-u, --url   set which URL to check"
	echo "-s, --successCode set which HTTP code resolves as OK"
	echo "-i, --interactive Will not curl URL to determined if the problem is fixed, but will wait user to manually check between plugin deactivations"
	echo "-r, --reset When finished set plugins status (active/inactive) as they were before execution"
	echo "-d, --deactivate While deactivating plugins 1 by 1, leave them deactivated instead or reactivating"
	echo "-e, --errorCode Instead of looking for sucessCode, check if HTTP return code is different from errorCode specified"
	exit 0
fi

checkPluginConflicts

if [[ reset -eq 1 ]]
then 
	echo "Reseting plugins status"
	resetPlugins
fi
